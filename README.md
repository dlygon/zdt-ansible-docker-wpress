# zdt-ansible-docker-wpress

My first project for ZeroDownTime - Ansible code for WordPress and DB installation in Docker containers. 

The current main playbook is zdt-wpress-raw.yaml and it requires docker-compose.yaml present in the project directory. 

The previous version called zdt-wpress-inline.yaml has been moved to the deprecated branch due to a bug in the Ansible module docker-compose. 
